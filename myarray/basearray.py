from typing import Tuple
from typing import List
from typing import Union
from operator import add,sub
import math


class BaseArray:
    """
    Razred BaseArray, za učinkovito hranjenje in dostopanje do večdimenzionalnih podatkov. Vsi podatki so istega
    številskega tipa: int ali float.

    Pri ustvarjanju podamo obliko-dimenzije tabele. Podamo lahko tudi tip hranjenih podatkov (privzeto je int) in
    podatke za inicializacijo (privzeto vse 0).

    Primeri ustvarjanja:
    a = BaseArray((10,)) # tabela 10 vrednosti, tipa int, vse 0
    a = BaseArray((3, 3), dtype=float) # tabela 9 vrednost v 3x3 matriki, tipa float, vse 0.0
    a = BaseArray((2, 3), dtype=float, dtype=float, data=(1., 2., 1., 2., 1., 2.)) # tabela 6 vrednost v 2x3 matriki,
                                                                               #tipa float, vrednosti 1. in 2.
    a = BaseArray((2, 3, 4, 5), data=tuple(range(120))) # tabela dimenzij 2x3x4x5, tipa int, vrednosti od 0 do 120


    Do podatkov dostopamo z operatorjem []. Pri tem moramo paziti, da uporabimo indekse, ki so znotraj oblike tabele.
    Začnemo z indeksom 0, zadnji indeks pa je dolžina dimenzije -1.
     Primeri dostopanja :

    a = BaseArray((2, 3, 4, 5))
    a[0, 1, 2, 1] = 10 # nastavimo vrednost 10 na mesto 0, 1, 2, 1
    v = a[0, 0, 0, 0] # preberemo vrednost na mestu 0, 0, 0 ,0

    a[0, 3, 0, 0] # neveljaven indeks, ker je vrednost 3 enaka ali večja od dolžine dimenzije

    Velikost tabele lahko preberemo z lastnostjo 'shape', podatkovni tip pa z 'dtype'. Primer:

    a.shape # to vrne vrednost  (2, 3, 4, 5)
    a.dtype # vrne tip int

    Čez tabelo lahko iteriramo s for zanko, lahko uporabimo operatorje/ukaze 'reversed' in 'in'.

    a = BaseArray((4,2), data=(1, 2, 3, 4, 5, 6, 7, 8))
    for v in a: # for zanka izpiše vrednost od 1 do 8
        print(v)
    for v in reversed(a): # for zanka izpiše vrednost od 8 do 1
        print(v)

    2 in a # vrne True
    -1 in a # vrne False
    """
    def __init__(self, shape: Tuple[int], dtype: type=None, data: Union[Tuple, List]=None):
        """
        Create an initialize a multidimensional myarray of a selected data type.
        Init the myarray to 0.

        :param shape: tuple or list of integers, shape of constructed myarray
        :param dtype: type of data stored in myarray, float or int
        :param data: list of tuple of data values, must contain consistent types and
                     have a length that matches the shape.
        """

        if not isinstance(shape, (tuple, list)):
            raise Exception(f'shape {shape:} is not a tuple or list')
        for v in shape:
            if not isinstance(v, int):
                raise Exception(f'shape {shape:} contains a non integer {v:}')

        self.__shape = (*shape,)

        self.__steps = _shape_to_steps(shape)

        if dtype is None:
            dtype = float
        if dtype not in [int, float]:
            raise Exception('Type must by int or float.')
        self.__dtype = dtype

        n = 1
        for s in self.__shape:
            n *= s

        if data is None:
            if dtype == int:
                self.__data = [0]*n
            elif dtype == float:
                self.__data = [0.0]*n
        else:
            if len(data) != n:
                raise Exception(f'shape {shape:} does not match provided data length {len(data):}')

            for d in data:
                if not isinstance(d, (int, float)):
                    raise Exception(f'provided data does not have a consistent type')
            self.__data = [d for d in data]

    @property
    def shape(self):
        """
        :return: a tuple representing the shape of the myarray.
        """
        return self.__shape

    @property
    def dtype(self):
        """
        :return: the type stored in the myarray.
        """
        return self.__dtype

    def __test_indice_in_range(self, ind):
        """
        Test if the given indice is in within the range of this myarray.
        :param ind: the indices used
        :return: True if indices are valid, False otherwise.
        """
        for ax in range(len(self.__shape)):
            if ind[ax] < 0 or ind[ax] >= self.shape[ax]:
                raise Exception('indice {:}, axis {:d} out of bounds (0, {:})'.format(ind, ax, self.__shape[ax]))

    def __getitem__(self, indice):
        """
        Return a value from this myarray.
        :param indice: indice to this myarray, integer or tuple
        :return: value at indice
        """
        if not type(indice) is tuple:
            indice = (indice,)

        if not _is_valid_indice(indice):
            raise Exception(f'{indice} is not a valid indice')

        if len(indice) != len(self.__shape):
            raise Exception(f'indice {indice} is not valid for this myarray')

        self.__test_indice_in_range(indice)
        key_lin = _multi_to_lin_ind(indice, self.__steps)
        return self.__data[key_lin]

    def __setitem__(self, indice, value):
        """
        Set a value in this myarray.
        :param indice: valued indice to a position, integer of tuple
        :param value: value to set
        :return: does not return
        """
        if not type(indice) is tuple:
            indice = (indice,)

        if not _is_valid_indice(indice):
            raise Exception(f'{indice} is not a valid indice')

        if len(indice) != len(self.__shape):
            raise Exception(f'indice {indice} is not valid for this myarray')

        self.__test_indice_in_range(indice)
        key_lin = _multi_to_lin_ind(indice, self.__steps)
        self.__data[key_lin] = value

    def __iter__(self):
        for v in self.__data:
            yield v

    def __reversed__(self):
        for v in reversed(self.__data):
            yield v

    def __contains__(self, item):
        return item in self.__data

    def __add__(self, other):
        if self.shape != other.shape:
            print("Wrong shape")
            exit(-1)
        res = BaseArray(self.shape, data=tuple(map(add, self, other)))
        return res

    def __sub__(self, other):
        if self.shape != other.shape:
            print("Wrong shape")
            exit(-1)
        res = BaseArray(self.shape, data=tuple(map(sub, self, other)))
        return res

    def __mul__(self , other:int):
        data = []
        for i in list(self):
            data.append(i*other)
        res = BaseArray(self.shape, data=tuple(data))
        return res


    def __mul__(self, other:float):
         data = []
         for i in list(self):
             data.append(i*other)
         res = BaseArray(self.shape, data=tuple(data))
         return res



def _multi_ind_iterator(multi_inds, shape):
    inds = multi_inds[0]
    s = shape[0]

    if type(inds) is slice:
        inds_itt = range(s)[inds]
    elif type(inds) is int:
        inds_itt = (inds,)

    for ind in inds_itt:
        if len(shape) > 1:
            for ind_tail in _multi_ind_iterator(multi_inds[1:], shape[1:]):
                yield (ind,)+ind_tail
        else:
            yield (ind,)


def _shape_to_steps(shape):
    dim_steps_rev = []
    s = 1
    for d in reversed(shape):
        dim_steps_rev.append(s)
        s *= d
    steps = tuple(reversed(dim_steps_rev))
    return steps


def _multi_to_lin_ind(inds, steps):
    i = 0
    for n, s in enumerate(steps):
        i += inds[n]*s
    return i


def _is_valid_indice(indice):
    if isinstance(indice, tuple):
        if len(indice) == 0:
            return False
        for i in indice:
            if not isinstance(i, int):
                return False
    elif not isinstance(indice, int):
        return False
    return True

def prettyPrint(arr: BaseArray):
    sdata = list(map(str, arr))
    longest = len(max(sdata, key=len))
    dim = arr.shape
    print(80*"-")
    if len(dim) == 1:
        for i in arr:
            spacingF = 5 + longest
            spacingB = 5 + longest
            if arr[i] < 0:
                spacingF -= 1
                if arr[i] <= -10:
                    spacingB = spacingB - len(str(arr[i])) + 2
            if arr[i] >= 10:
                spacingB = spacingB - len(str(arr[i])) + 1
            print(spacingF * " ", end="")
            print(arr[i], end="")
            print(spacingB * " ", end="")
        print()
    elif len(dim) == 2:
        column = dim[0]
        row = dim[1]
        for i in range(0,column):
            for j in range(0,row):
                spacingF = 5 + longest
                spacingB = 5 + longest
                if arr[i, j] < 0:
                    spacingF -= 1
                    if arr[i, j] <= -10:
                        spacingB = spacingB - len(str(arr[i, j])) + 2
                if arr[i, j] >= 10:
                    spacingB = spacingB - len(str(arr[i, j])) + 1
                print(spacingF*" ", end="")
                print(arr[i,j], end="")
                print(spacingB* " ",end="")
            print()
    elif len(dim) == 3:
        column = dim[0]
        row = dim[1]
        Z = dim[2]
        for i in range(0,column):
            for j in range(0,row):
                for z in range(0, Z):
                    spacingF = 5 +longest
                    spacingB = 5 +longest
                    if arr[i,j,z]<0:
                        spacingF-=1
                        if arr[i,j,z]<=-10:
                            spacingB = spacingB - len(str(arr[i, j, z])) + 2
                    if arr[i,j,z]>=10:
                        spacingB = spacingB - len(str(arr[i, j, z]))+1
                    print(spacingF*" ", end="")
                    print(arr[i,j,z], end="")
                    print(spacingB* " ",end="")
                print()
            print("\n next Dim Z:",i)
    return BaseArray


def mysort(list):
    if len(list)>1:
        mid = len(list)//2
        lefthalf = list[:mid]
        righthalf = list[mid:]

        mysort(lefthalf)
        mysort(righthalf)

        i = 0
        j = 0
        k = 0

        while i < len(lefthalf) and j < len(righthalf):
            if lefthalf[i] < righthalf[j]:
                list[k] = lefthalf[i]
                i = i + 1
            else:
                list[k] = righthalf[j]
                j = j + 1
            k = k + 1

        while i < len(lefthalf):
            list[k] = lefthalf[i]
            i = i + 1
            k = k + 1

        while j < len(righthalf):
            list[k] = righthalf[j]
            j = j + 1
            k = k + 1

    return list


def mergeSort(arr: BaseArray, mode):
    if mode == 0:
        dim = arr.shape
        if len(dim) == 1:
            x = dim[0]
            slice = []
            j=0
            while j < x:
                slice.append(arr[j])
                j += 1
            tmp = mysort(slice)
            arr = BaseArray((x,),data = tuple(tmp))
            print(tmp)
        elif len(dim) == 2:
            data =[]
            x = dim[0]
            y = dim[1]
            alldim = len(dim)
            i = 0
            while i < x:
                slice = []
                j = 0
                while j < y:
                    slice.append(arr[i, j])
                    j += 1
                i += 1
                data+=(mysort(slice))
            arr = BaseArray((x,y), data=tuple(data))
            #print(list(arr))
    elif mode == 1:
        dim = arr.shape
        if len(dim) == 1:
            x = dim[0]
            slice = []
            j=0
            while j < x:
                slice.append(arr[j])
                j += 1
            tmp = mysort(slice)
            arr = BaseArray((x,), data=tuple(tmp))
            print(tmp)
        elif len(dim) == 2:
            data = []
            x = dim[0]
            y = dim[1]
            for c in range(0, y):
                slice = []
                for r in range(0, x):
                    slice.append(arr[r, c])
                data += (mysort(slice))
            data2 = groupColimns(data, x)
            arr = BaseArray((x,y), data=tuple(data2))
            #print(list(arr))
        else:
            print("Dimensions too big")
    else:
        print("Wrong mode")
        raise Exception("Wrong mode")
    return arr


def groupColimns(data, x):
    ret =[]
    for j in range(0, x):
        ret+= list(data[i:i + 1][0] for i in range(j, len(data), x))
    return ret


def search(arr: BaseArray, searchVal):
    dim = arr.shape
    if len(dim) == 1:
        x = dim[0]
        indexSearch = [i for i, X in enumerate(arr) if X == searchVal]
        res = tuple(indexSearch)
    elif len(dim) == 2:
        x = dim[0]
        y = dim[1]
        res =[]
        for r in range(0,x):
            slice = []
            for c in range(0,y):
                slice.append(arr[r,c])
            indexSearch = [i for i, X in enumerate(list(slice))if X == searchVal]
            for d in indexSearch:
                res.append(tuple([r, d]))
        res = tuple(res)
    elif len(dim) == 3:
        x = dim[0]
        y = dim[1]
        z = dim[2]
        res =[]
        for r in range(0, x):
            for c in range(0, y):
                slice = []
                for Z in range(0, z):
                    slice.append(arr[r, c, Z])
                indexSearch = [i for i, X in enumerate(list(slice))if X == searchVal]
                for d in indexSearch:
                    res.append(tuple([c, d, r]))
        res = tuple(res)

    else:
        print("matrix is too big")
    return res


def multiply(A: BaseArray, B: BaseArray):
    res = []
    data = []
    if len(A.shape) == 1 and len(B.shape) == 1:
        w, h = B.shape[0], A.shape[0]
        matrix = [[0 for x in range(w)] for y in range(h)]
        ADim = A.shape[0]
        BDim = B.shape[0]
        for i in range(0, int(A.shape[0])):
            for j in range (0, B.shape[0]):
                for k in range(0,ADim):
                    bData = B[k, j]
                    aData = A[i, k]
                    matrix[i][j] += aData * bData
        data = [x for sublist in matrix for x in sublist]
        res = BaseArray((A.shape[0],), data=tuple(data))
    elif len(A.shape) == 2 and len(B.shape) == 2:
        ADim = A.shape[1]
        BDim = B.shape[0]
        if ADim != BDim:
            print("wrong dimensions")
            raise Exception("Wrong dimensions")
        #notranje dimenzije se ujemajo
        w,h = B.shape[1], A.shape[0]
        matrix = [[0 for x in range(w)] for y in range(h)]

        for i in range(0, int(A.shape[0])):
            for j in range (0, B.shape[1]):
                for k in range(0,ADim):
                    if(k > B.shape[0] or  j > B.shape[1]):
                        bData = 0
                    else:
                        bData = B[k, j]
                    if(i > A.shape[0] or  k > A.shape[0]):
                        aData = 0
                    else:
                        aData = A[i, k]
                    matrix[i][j] += aData * bData
        data = [x for sublist in matrix for x in sublist]
        res = BaseArray((A.shape[0], B.shape[1]), data=tuple(data))
    else:
        print("Matrix too big")
        raise Exception("Matrix too big")
    return res


def exp(arr: BaseArray):
    data = []
    for i in list(arr):
        data.append(math.exp(i))
    res = BaseArray(arr.shape, data=tuple(data))
    return res


def log(arr:BaseArray):
    data = []
    for i in list(arr):
        data.append(math.log(i, 10))
    res = BaseArray(arr.shape, data=tuple(data))
    return res


def dot(A:BaseArray, B:BaseArray):
    if A.shape != B.shape:
        print("Wrong dimensions")
        raise Exception("Wrong dimensions")
    data = 0
    for i in range(0, len(list(A))):
        data += (A[i]*B[i])
    return data

