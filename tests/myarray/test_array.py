from unittest import TestCase
from myarray.basearray import BaseArray
import myarray.basearray as ndarray


class TestArray(TestCase):
    def test___init__(self):

        # simple init tests, other things are tester later
        try:
            a = BaseArray((2,))
            a = BaseArray([2, 5])
            a = BaseArray((2, 5), dtype=int)
            a = BaseArray((2, 5), dtype=float)
        except Exception as e:
            self.fail(f'basic constructor test failed, with exception {e:}')

        # test invalid parameters as shape
        with self.assertRaises(Exception) as cm:
            a = BaseArray(5)
        self.assertEqual(('shape 5 is not a tuple or list',),
                         cm.exception.args)

        # test invalid value in shape
        with self.assertRaises(Exception) as cm:
            a = BaseArray((4, 'a'))
        self.assertEqual(('shape (4, \'a\') contains a non integer a',),
                         cm.exception.args)

        # TODO test invalid dtype

        # test valid data param, list, tuple
        a = BaseArray((3, 4), dtype=int, data=list(range(12)))
        a = BaseArray((3, 4), dtype=int, data=tuple(range(12)))

        # test invalid data param
        # invalid data length
        with self.assertRaises(Exception) as cm:
            a = BaseArray((2, 4), dtype=int, data=list(range(12)))
        self.assertEqual(('shape (2, 4) does not match provided data length 12',),
                         cm.exception.args)

        # inconsistent data type
        with self.assertRaises(Exception) as cm:
            a = BaseArray((2, 2), dtype=int, data=(4, 'a', (), 2.0))
        self.assertEqual(('provided data does not have a consistent type',),
                         cm.exception.args)

    def test_shape(self):
        a = BaseArray((5,))
        self.assertTupleEqual(a.shape, (5,))
        a = BaseArray((2, 5))
        self.assertTupleEqual(a.shape, (2, 5))
        a = BaseArray((3, 2, 5))
        self.assertEqual(a.shape, (3, 2, 5))

    def test_dtype(self):
        # test if setting a type returns the set type
        a = BaseArray((1,))
        self.assertIsInstance(a[0], float)
        a = BaseArray((1,), int)
        self.assertIsInstance(a[0], int)
        a = BaseArray((1,), float)
        self.assertIsInstance(a[0], float)

    def test_get_set_item(self):
        # test if setting a value returns the value
        a = BaseArray((1,))
        a[0] = 1
        self.assertEqual(1, a[0])
        # test for multiple dimensions
        a = BaseArray((2, 1))
        a[0, 0] = 1
        a[1, 0] = 1
        self.assertEqual(1, a[0, 0])
        self.assertEqual(1, a[1, 0])

        # test with invalid indices
        a = BaseArray((2, 2))
        with self.assertRaises(Exception) as cm:
            a[3, 0] = 0
        self.assertEqual(('indice (3, 0), axis 0 out of bounds (0, 2)',),
                         cm.exception.args)

        # test invalid type of ind
        with self.assertRaises(Exception) as cm:
            a[1, 1.1] = 0
        self.assertEqual(cm.exception.args,
                         ('(1, 1.1) is not a valid indice',))

        with self.assertRaises(Exception) as cm:
            a[1, 'a'] = 0
        self.assertEqual(('(1, \'a\') is not a valid indice',),
                         cm.exception.args)

        # test invalid number of ind
        with self.assertRaises(Exception) as cm:
            a[1] = 0
        self.assertEqual(('indice (1,) is not valid for this myarray',),
                         cm.exception.args)

        # test data intialized via data parameter
        a = BaseArray((2, 3, 4), dtype=int, data=tuple(range(24)))
        self.assertEqual(3, a[0, 0, 3])
        self.assertEqual(13, a[1, 0, 1])

        # TODO enter invalid type


    def test_iter(self):
        a = BaseArray((2, 3), dtype=int)
        a[0, 0] = 1
        a[0, 1] = 2
        a[0, 2] = 3
        a[1, 0] = 4
        a[1, 1] = 5
        a[1, 2] = 6

        a_vals = [v for v in a]
        self.assertListEqual([1, 2, 3, 4, 5, 6], a_vals)

    def test_reversed(self):
        a = BaseArray((2, 3), dtype=int)
        a[0, 0] = 1
        a[0, 1] = 2
        a[0, 2] = 3
        a[1, 0] = 4
        a[1, 1] = 5
        a[1, 2] = 6

        a_vals = [v for v in reversed(a)]
        self.assertListEqual([6, 5, 4, 3, 2, 1], a_vals)

    def test_contains(self):
        a = BaseArray((1,), dtype=float)
        a[0] = 1

        self.assertIn(1., a)
        self.assertIn(1, a)
        self.assertNotIn(0, a)

    def test_shape_to_steps(self):
        shape = (4, 2, 3)
        steps_exp = (6, 3, 1)
        steps_res = ndarray._shape_to_steps(shape)
        self.assertTupleEqual(steps_exp, steps_res)

        shape = [1]
        steps_exp = (1,)
        steps_res = ndarray._shape_to_steps(shape)
        self.assertTupleEqual(steps_exp, steps_res)

        shape = []
        steps_exp = ()
        steps_res = ndarray._shape_to_steps(shape)
        self.assertTupleEqual(steps_exp, steps_res)

    def test_multi_to_lin_ind(self):
        # shape = 4, 2,
        # 0 1
        # 2 3
        # 4 5
        # 6 7
        steps = 2, 1
        multi_lin_inds_pairs = (((0, 0), 0),
                                ((1, 1), 3),
                                ((3, 0), 6),
                                ((3, 1), 7))
        for multi_inds, lin_ind_exp in multi_lin_inds_pairs:
            lin_ind_res = ndarray._multi_to_lin_ind(multi_inds, steps)
            self.assertEqual(lin_ind_exp, lin_ind_res)

        # shape = 2, 3, 4
        #
        # 0 1 2 3
        # 4 5 6 7
        #
        #  8  9 10 11
        # 12 13 14 15
        steps = 8, 4, 1
        multi_lin_inds_pairs = (((0, 0, 0), 0),
                                ((1, 1, 0), 12),
                                ((0, 0, 3), 3),
                                ((1, 1, 2), 14))

        for multi_inds, lin_ind_exp in multi_lin_inds_pairs:
            lin_ind_res = ndarray._multi_to_lin_ind(multi_inds, steps)
            self.assertEqual(lin_ind_res, lin_ind_exp)

    def test_is_valid_instance(self):
        self.assertTrue(ndarray._is_valid_indice(1))
        self.assertTrue(ndarray._is_valid_indice((1,0)))

        self.assertFalse(ndarray._is_valid_indice(()))
        self.assertFalse(ndarray._is_valid_indice([1]))
        self.assertFalse(ndarray._is_valid_indice(1.0))
        self.assertFalse(ndarray._is_valid_indice((1, 2.)))

    def test_multi_ind_iterator(self):
        shape = (30,)
        ind = (slice(10, 20, 3),)
        ind_list_exp = ((10,),
                        (13,),
                        (16,),
                        (19,))
        ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
        for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
            self.assertTupleEqual(ind_exp, ind_res)

        shape = (5, 5, 5)
        ind = (2, 2, 2)
        ind_list_exp = ((2, 2, 2),)
        ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
        for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
            self.assertTupleEqual(ind_exp, ind_res)

        ind = (2, slice(3), 2)
        ind_list_exp = ((2, 0, 2),
                        (2, 1, 2),
                        (2, 2, 2),
                        )
        ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
        for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
            self.assertTupleEqual(ind_exp, ind_res)

        ind = (slice(None, None, 2), slice(3), slice(2, 4))
        ind_list_exp = ((0, 0, 2), (0, 0, 3),
                        (0, 1, 2), (0, 1, 3),
                        (0, 2, 2), (0, 2, 3),
                        (2, 0, 2), (2, 0, 3),
                        (2, 1, 2), (2, 1, 3),
                        (2, 2, 2), (2, 2, 3),
                        (4, 0, 2), (4, 0, 3),
                        (4, 1, 2), (4, 1, 3),
                        (4, 2, 2), (4, 2, 3),
                        )
        ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
        for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
            self.assertTupleEqual(ind_exp, ind_res)

    def test_prettyPrint(self):
        ndarray.prettyPrint(BaseArray((3,), data=tuple(range(0, 3))))

        ndarray.prettyPrint(BaseArray((3, 4), data=tuple(range(5, 17))))
        ndarray.prettyPrint(BaseArray((2, 2), data=(0., 0.3, 0.5, 5.5)))
        ndarray.prettyPrint(BaseArray((3, 3), data=tuple(range(6, 15))))
        ndarray.prettyPrint(BaseArray((3, 3, 3), data=tuple(range(-12, 15))))

    def test_sort(self):
        res = ndarray.mergeSort(BaseArray((3,), data=(3, 1, 2)), 0)
        exp = BaseArray((3,), data=tuple(range(1, 4)))
        self.assertEqual(list(exp), list(res))

        res = ndarray.mergeSort(BaseArray((3,3), data=(6., 5., 7., 8., 9., 10., 11., 13., 12.)), 0)
        exp = BaseArray((3,3), data=tuple(range(5,14)))
        self.assertEqual(list(exp), list(res))

        res = ndarray.mergeSort(BaseArray((3,4), data=(6., 5., 7., 8., 9., 10., 12., 11., 14., 13., 15., 16)), 0)
        exp = BaseArray((3,4), data=tuple(range(5,17)))
        self.assertEqual(list(exp), list(res))

        #vrstice
        res = ndarray.mergeSort(BaseArray((3,), data=(3, 1, 2)), 1)
        exp = BaseArray((3,), data=tuple(range(1, 4)))
        self.assertEqual(list(exp), list(res))

        res = ndarray.mergeSort(BaseArray((3, 3), data=(0., 1., 1., 4., 0., -2., 1., 3., 0.)), 1)
        exp = BaseArray((3, 3), data=(0., 0., -2., 1., 1., 0., 4., 3., 1.))
        self.assertEqual(list(exp), list(res))

        res = ndarray.mergeSort(BaseArray((3, 4), data=(0., 1., 1., 5., 4., 0., -2., 1., 1., 3., 0., 0.)), 1)
        exp = BaseArray((3, 4), data=(0., 0., -2., 0., 1., 1., 0., 1., 4., 3., 1., 5.))
        self.assertEqual(list(exp), list(res))

        try:
            res = ndarray.mergeSort(BaseArray((3, 3), data=(6., 5., 7., 8., 9., 10., 11., 13., 12.)), -1)
        except BaseException as e:
            exp = Exception("Wrong mode")
            self.assertEqual(exp.args[0], e.args[0])

    def test_search(self):
        # 1D
        res = ndarray.search(BaseArray((5,), data=(1., 2., 3., 2., 2.)), 2)
        exp = tuple([1, 3, 4])
        self.assertEqual(exp, res)

        #empty
        res = ndarray.search(BaseArray((5,), data=(1., 2., 3., 2., 2.)), 5)
        exp = tuple([])
        self.assertEqual(exp, res)

        #2D
        #solski
        res = ndarray.search(BaseArray((3, 3), data=(0., 0., 0., 2., 0., 0., 0., 2., 2.,)), 2)
        exp = ((1, 0), (2, 1), (2, 2))
        self.assertEqual(exp, res)

        #empty
        res = ndarray.search(BaseArray((3, 3), data=(0., 0., 0., 2., 0., 0., 0., 2., 2.,)), 10)
        exp = tuple([])
        self.assertEqual(exp, res)



        #3D
        arr = BaseArray((3, 3, 3), data=(0., 0., 0., 2., 0., 0., 0., 2., 2., 1., 2., 3., 4., 5., 6., 7.,
                                                        8., 9., 10., 11., 12., 12., 13., 14., 15., 16., 17.))
        res = ndarray.search(arr, 2)
        exp = ((1, 0, 0), (2, 1, 0), (2, 2, 0), (0, 1, 1))
        self.assertEqual(exp, res)

        arr = BaseArray((3, 3, 3), data=(0., 0., 0., 2., 0., 0., 0., 2., 2., 1., 3., 3., 4., 5., 6., 7.,
                                                        8., 9., 10., 11., 12., 12., 13., 14., 15., 16., 2.))
        res = ndarray.search(arr, 2)
        exp = ((1, 0, 0), (2, 1, 0), (2, 2, 0), (2, 2, 2))
        self.assertEqual(exp, res)

        #empty
        res = ndarray.search(arr, 999)
        exp = tuple([])
        self.assertEqual(exp, res)

    def test_addition(self):
        A = BaseArray((5,), data=tuple(range(0, 5)))
        B = BaseArray((5,), data=tuple(range(0, 5)))
        res = A + B
        exp = BaseArray((5,), data=(0, 2, 4, 6, 8))
        self.assertEqual(list(exp), list(res))

        A = BaseArray((2,2), data=tuple(range(0, 4)))
        B = BaseArray((2,2), data=tuple(range(0, 4)))
        res = A+B
        exp = BaseArray((2,2), data=(0, 2, 4, 6,))
        self.assertEqual(list(exp), list(res))

        A = BaseArray((3,3), data=tuple(range(0, 9)))
        B = BaseArray((3,3), data=tuple(range(0, 9)))
        res = A + B
        exp = BaseArray((3,3), data=(0, 2, 4, 6, 8, 10, 12, 14, 16))
        self.assertEqual(list(exp), list(res))

        A = BaseArray((3,3,3), data=tuple(range(0, 27)))
        B = BaseArray((3,3,3), data=tuple(range(0, 27)))
        res = A + B
        exp = BaseArray((3,3,3), data=(0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36, 38, 40, 42,
                                     44, 46, 48, 50, 52))
        self.assertEqual(list(exp), list(res))

    def test_subtraction(self):
        A = BaseArray((5,), data=tuple(range(0, 5)))
        B = BaseArray((5,), data=tuple(range(0, 5)))
        res = A - B
        exp = BaseArray((5,), data=(0, 0, 0, 0, 0))
        self.assertEqual(list(exp), list(res))

        A = BaseArray((2,2), data=tuple(range(1, 5)))
        B = BaseArray((2,2), data=tuple(range(0, 4)))
        res = A - B
        exp = BaseArray((2,2), data=(1, 1, 1, 1,))
        self.assertEqual(list(exp), list(res))

        A = BaseArray((3, 3), data=tuple(range(2, 11)))
        B = BaseArray((3, 3), data=tuple(range(0, 9)))
        res = A - B
        exp = BaseArray((3, 3), data=(2, 2, 2, 2, 2, 2, 2, 2, 2))
        self.assertEqual(list(exp), list(res))

        A = BaseArray((3,3,3), data=tuple(range(0, 27)))
        B = BaseArray((3,3,3), data=tuple(range(0, 27)))
        res = A - B
        exp = BaseArray((3,3,3), data=(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
        self.assertEqual(list(exp), list(res))

    def test_scalar(self):
        A = BaseArray((5,), data=tuple(range(0, 5)))
        B = 5
        res = A * B
        exp = BaseArray((5,), data=(0, 5, 10, 15, 20))
        self.assertEqual(list(exp), list(res))

        A = BaseArray((2, 2), data=tuple(range(1, 5)))
        B = 5
        res = A * B
        exp = BaseArray((2, 2), data=(5, 10, 15, 20,))
        self.assertEqual(list(exp), list(res))

        A = BaseArray((2, 3), data=tuple(range(1, 7)))
        B = 5
        res = A * B
        exp = BaseArray((2, 3), data=(5, 10, 15, 20, 25, 30))
        self.assertEqual(list(exp), list(res))

        #decimalke
        A = BaseArray((5,), data=tuple(range(0, 5)))
        B = 1.5
        res = A * B
        exp = BaseArray((5,), data=(0.0, 1.5, 3.0, 4.5, 6.0))
        self.assertEqual(list(exp), list(res))

        A = BaseArray((2, 2), data=tuple(range(1, 5)))
        B = 1.5
        res = A * B
        exp = BaseArray((2, 2), data=(1.5, 3.0, 4.5, 6.0))
        self.assertEqual(list(exp), list(res))

        A = BaseArray((2, 3), data=tuple(range(1, 7)))
        B = 1.5
        res = A * B
        exp = BaseArray((2, 3), data=(1.5, 3.0, 4.5, 6.0, 7.5, 9.0))
        self.assertEqual(list(exp), list(res))

    def test_mul(self):
        A = BaseArray((3, 3), data=tuple(range(0, 9)))
        B = BaseArray((3, 3), data=tuple(range(0, 9)))
        res = ndarray.multiply(A,B)
        exp = BaseArray((3, 3), data=(15, 18, 21, 42, 54, 66, 69, 90, 111))
        self.assertEqual(list(exp), list(res))

        A = BaseArray((2, 3), data=tuple(range(0, 6)))
        B = BaseArray((3, 3), data=tuple(range(0, 9)))
        res = ndarray.multiply(A,B)
        exp = BaseArray((2, 3), data=(15, 18, 21, 42, 54, 66))
        self.assertEqual(list(exp), list(res))

        A = BaseArray((2, 3), data=tuple(range(0, 6)))
        B = BaseArray((3, 2), data=tuple(range(0, 6)))
        res = ndarray.multiply(A,B)
        exp = BaseArray((2, 2), data=(10, 13, 28, 40))
        self.assertEqual(list(exp), list(res))


        A = BaseArray((3, 2), data=tuple(range(0, 6)))
        B = BaseArray((3, 2), data=tuple(range(0, 6)))
        try:
            res = ndarray.multiply(A, B)
        except BaseException as e:
            exp = Exception('Wrong dimensions')
            self.assertEqual(exp.args[0], e.args[0])


        A = BaseArray((3, 2, 2), data=tuple(range(0, 12)))
        B = BaseArray((3, 2), data=tuple(range(0, 6)))
        try:
            res = ndarray.multiply(A, B)
        except BaseException as e:
            exp = Exception('Matrix too big')
            self.assertEqual(exp.args[0], e.args[0])




    def test_dot(self):
        A = BaseArray((3,), data=(1, 3, -5))
        B = BaseArray((3,), data=(4, -2, -1))
        res = ndarray.dot(A,B)
        self.assertEqual(3, res)

        A = BaseArray((5,), data=tuple(range(0, 5)))
        B = BaseArray((5,), data=tuple(range(5, 10)))
        res = ndarray.dot(A, B)
        self.assertEqual(80, res)

        A = BaseArray((3, 2, 2), data=tuple(range(0, 12)))
        B = BaseArray((3, 2), data=tuple(range(0, 6)))
        try:
            res = ndarray.dot(A, B)
        except BaseException as e:
            exp = Exception('Wrong dimensions')
            self.assertEqual(exp.args[0], e.args[0])

    def test_exp(self):
        A = BaseArray((5,), data=tuple(range(0, 5)))
        res = ndarray.exp(A)
        exp = BaseArray((5,), data=(1.0, 2.718281828459045, 7.38905609893065, 20.085536923187668, 54.598150033144236))
        self.assertEqual(list(exp), list(res))

        A = BaseArray((2, 2), data=tuple(range(1, 5)))
        res = ndarray.exp(A)
        exp = BaseArray((2, 2), data=(2.718281828459045, 7.38905609893065, 20.085536923187668, 54.598150033144236))
        self.assertEqual(list(exp), list(res))

        A = BaseArray((2, 3), data=tuple(range(1, 7)))
        res = ndarray.exp(A)
        exp = BaseArray((2, 3), data=(2.718281828459045, 7.38905609893065, 20.085536923187668, 54.598150033144236,
                                      148.4131591025766, 403.4287934927351))
        self.assertEqual(list(exp), list(res))

    def test_log(self):
        A = BaseArray((5,), data=tuple(range(1, 6)))
        res = ndarray.log(A)
        exp = BaseArray((5,), data=(0, 0.30102999566398114, 0.47712125471966244, 0.6020599913279623, 0.6989700043360187))
        self.assertEqual(list(exp), list(res))

        A = BaseArray((2, 2), data=tuple(range(1, 5)))
        res = ndarray.log(A)
        exp = BaseArray((2, 2), data=(0.0, 0.30102999566398114, 0.47712125471966244, 0.6020599913279623))
        self.assertEqual(list(exp), list(res))

        A = BaseArray((2, 3), data=tuple(range(1, 7)))
        res = ndarray.log(A)
        exp = BaseArray((2, 3), data=(0.0, 0.30102999566398114, 0.47712125471966244, 0.6020599913279623,
                                      0.6989700043360187, 0.7781512503836435))
        self.assertEqual(list(exp), list(res))

